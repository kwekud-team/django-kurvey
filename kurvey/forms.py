from django import forms
import django_filters

from kurvey.models import QuestionSet, Capture


class QuestionImporterForm(forms.Form):
    question_set_pk = forms.ModelChoiceField(queryset=QuestionSet.objects.all())

    def __init__(self, *args, **kwargs):
        super(QuestionImporterForm, self).__init__(*args, **kwargs)

    def clean_question_set_pk(self):
        return self.cleaned_data['question_set_pk'].pk


class CaptureAuthForm(forms.Form):
    email = forms.EmailField()


class MyDateRangeWidget(django_filters.widgets.DateRangeWidget):

    def __init__(self, from_attrs=None, to_attrs=None, attrs=None):
        super(MyDateRangeWidget, self).__init__(attrs)
        if from_attrs:
            self.widgets[0].attrs.update(from_attrs)
        if to_attrs:
            self.widgets[1].attrs.update(to_attrs)


class CaptureListFilterForm(django_filters.FilterSet):
    user__username = django_filters.CharFilter(required=False, label='User')
    date_created = django_filters.DateFromToRangeFilter(required=False)
    date_completed = django_filters.DateFromToRangeFilter(required=False)

    class Meta:
        model = Capture
        fields = ['question_set', 'user__username', 'date_created', 'date_completed']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._init_fields_display()
        qs = self.form.fields['question_set'].queryset
        if qs.count() == 1:
            del self.form.fields['question_set']

    def _init_fields_display(self):
        for field_name in self.form.fields:
            if self.form.fields[field_name].widget.__class__.__name__ == 'Select':
                klass = 'form-select'
            else:
                klass = 'form-control'
            self.form.fields[field_name].widget.attrs.update({
                'class': klass
            })

        self.form.fields['date_created'].widget = django_filters.widgets.DateRangeWidget(
            attrs={
                'class': 'form-control date-picker',
                'autocomplete': 'off',
            }
        )

        self.form.fields['date_completed'].widget = django_filters.widgets.DateRangeWidget(
            attrs={
                'class': 'form-control date-picker',
                'autocomplete': 'off',
            }
        )