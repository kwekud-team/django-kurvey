from django import forms


class VForm(forms.Form):

    def __init__(self, *args, **kwargs):
        info = kwargs.pop('info', [])
        vform_fields = kwargs.pop('fields', {})
        super(VForm, self).__init__(*args, **kwargs)

        self.info = info

        for vform_field in vform_fields:
            field = vform_field.field
            field.label = vform_field.label
            field.required = vform_field.is_required
            field.help_text = vform_field.help_text
            field.initial = vform_field.initial
            field.widget = vform_field.widget

            field.extra = vform_field.extra
            non_fields_args = {"attrs": {"row": 3, "cols": 20}}
            # non_fields_args = vform_field.extra.get('widget', {})
            classes = f'{field.__class__.__name__} {vform_field.extra.widget_class}'
            non_fields_args['class'] = classes

            field.widget.attrs.update(non_fields_args)

            self.fields[vform_field.key] = field
