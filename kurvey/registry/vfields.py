import os
import json
from django import forms
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.files.uploadedfile import UploadedFile
from django.template.defaultfilters import slugify

from kommons.utils.generic import intify, jsonify
from kurvey.registry.widgets import FormsetWidget
from kurvey.models import Question


class VField(forms.Field):
    empty_value = None
    widget_class = 'form-control'

    def __init__(self, question, capture_answer=None, *args, **kwargs):
        self.question = question
        self.capture_answer = capture_answer
        self.capture = (capture_answer.capture if capture_answer else None)

        self._extra = self.get_extra()
        super(VField, self).__init__(*args, **kwargs)

    def get_form_kwargs(self):
        val = {
            'required': self.question.is_required,
            'initial': self.get_initial_value(),
            'label': self.question.long_text or self.question.short_text,
            'help_text': self.question.help_text or '',
            'attrs': {'sup': '1'}
        }
        return val

    def get_widget(self):
        return self.widget

    def get_widget_class(self):
        return self.widget_class

    def get_value(self):
        return self.capture_answer.value if self.capture_answer else self.empty_value

    def get_initial_value(self):
        return self.get_value()

    def get_extra(self):
        return self.question.extra

    def get_display_value(self, value=None):
        return value or self.get_value() or self.empty_value

    def post_save(self):
        pass


class VCharField(forms.CharField, VField):
    pass


class VTextField(forms.CharField, VField):
    widget = forms.Textarea


class VEmailField(forms.EmailField, VField):
    pass


class VChoiceField(forms.ChoiceField, VField):
    append_empty = True

    def __init__(self, *args, **kwargs):
        super(VChoiceField, self).__init__(*args, **kwargs)
        self.choices = self.get_choices()

    def get_choices(self):
        choices = ([('', '---')] if self.append_empty else [])

        choices_dt = self._extra.get('choices', {})
        choices_source = choices_dt.get('source', '')
        choices_value = choices_dt.get('value', '')

        if choices_source == 'COMMA_SEPARATED':
            xs = choices_value.split(',')
            choices.extend([(x.strip(), x.strip()) for x in xs if x.strip()])

        elif choices_source == 'NEWLINE_LIST':
            xs = choices_value.split('\n')
            choices.extend([(x.strip(), x.strip()) for x in xs if x.strip()])

        return tuple(choices)


class VMultipleChoiceField(forms.MultipleChoiceField, VChoiceField):
    widget = forms.CheckboxSelectMultiple
    append_empty = False
    widget_class = ''
    separator = '|'

    def clean(self, value):
        value = super().clean(value)
        if value:
            return self.separator.join(value)
        return value

    def get_value(self, value=None):
        value = super().get_value() or ''
        return value.split(self.separator)

    def get_display_value(self, value=None):
        value = self.get_value()
        return ', '.join(value)


class VDateField(forms.DateField, VField):
    pass


class VFileField(forms.FileField, VField):
    upload_to = 'kurvey/'

    def get_display_value(self, value=None):
        value = self.get_value()

        if value:
            url = f'{settings.MEDIA_URL}{value}'
            html = f'<a href="{url}">View</a>'

            return mark_safe(html)

    def post_save(self):
        site = self.capture_answer.capture.question_set.site
        slug = self.capture_answer.capture.question_set.slug
        qname = self.question.pk
        value = self.capture_answer.value

        if isinstance(value, UploadedFile):
            file_xs = value.name.split('.')
            ext = file_xs[-1]
            fname = '.'.join(file_xs[:-1])
            filename = f'{slugify(fname)}.{ext}'

            path = f'{self.upload_to}{site.pk}/{slug}/{qname}/{filename}'
            actual_path = os.path.join(settings.MEDIA_ROOT, path)

            default_storage.save(actual_path, value)

            self.capture_answer.value = path
            self.capture_answer.save(update_fields=['value'])


class VImageField(forms.ImageField, VFileField):

    def get_display_value(self, value=None):
        value = self.get_value()

        if value:
            url = f'{settings.MEDIA_URL}{value}'
            html = f'<img src="{url}" style="width: 120px; height: 120px" />'

            return mark_safe(html)


class VFormset(VField):
    widget = FormsetWidget

    def __init__(self, question, capture_answer=None, *args, **kwargs):
        self.question = question
        self.capture_answer = capture_answer
        self.capture = (capture_answer.capture if capture_answer else None)

        self._extra = self.get_extra()

        kwargs['widget'] = self.get_widget_()
        super(VFormset, self).__init__(question, capture_answer=capture_answer, *args, **kwargs)

    def get_widget_(self):
        widget = self.widget()
        widget.vfield = self
        widget.expected_questions = self.get_expected_questions()
        return widget

    def get_expected_questions(self):
        formset_dt = self._extra.get('formset', {})
        formset_source = formset_dt.get('source', None)
        formset_filters = formset_dt.get('filters', {})

        qs = Question.objects.none()

        if formset_source == 'QUERYSET':
            qs = Question.objects.filter(parent=self.question, is_active=True)
            qs = qs.filter(**formset_filters)

        return qs

    def get_initial_value(self):
        value = super(VFormset, self).get_initial_value()
        val = None

        json_val = jsonify(value)
        if not json_val and self.question:
            init_val = self.question.initial_value
            json_val = jsonify(init_val)

        if json_val:
            val = []
            for row in json_val:
                tmp = {}
                for obj in self.get_expected_questions():
                    key = 'field__%s' % obj.pk
                    v = row.get(obj.slug, "") or row.get(key, "")
                    tmp[key] = v
                val.append(tmp)

        return val

    def clean(self, value):
        val_str = json.dumps(value)
        return val_str

    def get_can_change(self):
        return self._extra.get('formset_can_change', True)

    def get_display_value(self, value=None):
        value = super(VFormset, self).get_display_value(value)
        html = self.empty_value or '---'

        json_value = jsonify(value)
        if json_value:
            pks = [x.replace('field__', '') for x in json_value[0]]
            pks = (x for x in pks if intify(x))
            header_xs = Question.objects.filter(pk__in=pks, is_active=True)

            body_xs = []
            for row in json_value:
                tmp = []
                for x in header_xs:
                    tmp.append({
                        'value': row['field__%s' % x.pk],
                        'extra': x.extra
                    })
                if tmp:
                    body_xs.append(tmp)

            if header_xs and body_xs:
                html = render_to_string('kurvey/values/formset_display.html',
                                        {'headers': header_xs, 'content': body_xs})

        return mark_safe(html)
