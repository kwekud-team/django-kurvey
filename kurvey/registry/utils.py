from django.utils.module_loading import import_string
from django.utils import timezone

from kurvey.models import Question, CaptureAnswer
from kurvey.registry.vforms import VForm
from kurvey.constants import KurveyK
from kurvey.registry.dclasses import VFormFieldDataC, VFormNonFieldDataC


class VFieldUtils:

    def get_vfield_class(self, question):
        path = f'kurvey.registry.vfields.{question.input_type}'
        try:
            cls = import_string(path)
        except ImportError:
            path = f'kurvey.registry.vfields.VCharField'
            cls = import_string(path)
        return cls

    def get_vfield(self, question, capture_answer):
        vfield_class = self.get_vfield_class(question)
        return vfield_class(question=question, capture_answer=capture_answer)

    def gen_vform_field(self, vfield) -> VFormFieldDataC:
        return VFormFieldDataC(
            field=vfield,
            key=f'field__{vfield.question.pk}',
            is_required=vfield.question.is_required,
            label=vfield.question.get_text(),
            help_text=vfield.question.help_text or '',
            initial=vfield.get_value(),
            widget=vfield.get_widget(),
            extra=VFormNonFieldDataC(
                code=vfield.question.number,
                widget_class=vfield.get_widget_class()
            )
        )


class SurveyCaptureUtils:

    def __init__(self, request, question_set, capture=None):
        self.request = request
        self.question_set = question_set
        self.capture = capture
        self.group = self.get_current_group()

        self.vfield_utils = VFieldUtils()

    def get_form(self, **kwargs):
        vform_fields = [self.vfield_utils.gen_vform_field(x) for x in self.get_vfields()]
        return VForm(info=self.get_form_info(), fields=vform_fields, **kwargs)

    def get_form_info(self):
        if self.group:
            name = self.group.short_text
            description = self.group.description
        else:
            name = self.question_set.name
            description = self.question_set.description

        return {
            'name': name,
            'description': description
        }

    def get_question_queryset(self):
        qs = Question.objects.active().filter(question_set=self.question_set,
                                              question_type=KurveyK.QT_QUESTION.value)
        if self.group:
            qs = qs.filter(parent=self.group)
        return qs

    def get_vfields(self):
        xs = []
        for x in self.get_question_queryset():
            capture_answer = CaptureAnswer.objects.filter(capture=self.capture, question=x).first()
            xs.append(self.vfield_utils.get_vfield(x, capture_answer))

        return xs

    def get_all_groups(self):
        return Question.objects.active().filter(question_set=self.question_set, question_type=KurveyK.QT_GROUP.value)

    def get_current_group(self):
        group = None

        group_pk = self.request.GET.get('group', None)
        if group_pk:
            group = Question.objects.filter(pk=group_pk).first()

        if not group:
            qs = self.get_all_groups()
            group = (qs[0] if qs.exists() else None)

        return group

    def get_group_info(self):
        dt = {}

        all_groups = self.get_all_groups().order_by('sort')
        if all_groups.exists():
            prev_group = all_groups.filter(sort__lt=self.group.sort).order_by('-sort').first()
            next_group = all_groups.filter(sort__gt=self.group.sort).order_by('sort').first()
            highest_group = all_groups.order_by('-sort').first()
            position = (self.group.sort / all_groups.count()) * 100

            dt = {
                'previous': prev_group,
                'current': self.group,
                'next': next_group,
                'highest': highest_group,
                'position': position,
                'all': all_groups,
            }

        return dt

    def get_capture_review(self):
        group_values = []
        capture_pictures = self.get_capture_pictures()

        for grp in self.get_all_groups():
            question_values = []
            qs = grp.get_descendants(include_self=True)

            for x in qs.filter(question_type=KurveyK.QT_QUESTION.value, is_hidden=False, is_active=True):
                capture_answer = CaptureAnswer.objects.filter(capture=self.capture, question=x).first()
                vfield = self.vfield_utils.get_vfield(x, capture_answer)
                value = vfield.get_display_value()

                pictures = []
                if vfield.capture_answer:
                    pictures = list(filter(lambda x: x['capture_answer'] == vfield.capture_answer.pk,
                                           capture_pictures))

                question_values.append({
                    'question': x,
                    'capture_answer': vfield.capture_answer,
                    'value': value,
                    'text': x.long_text or x.short_text,
                    'pictures': pictures
                })

            if question_values:
                group_values.append({
                    'group': grp,
                    'question_values': question_values
                })
        return group_values

    def get_capture_pictures(self):
        return []

    def mark_capture_as_complete(self):
        self.capture.date_completed = timezone.now()
        self.capture.save(update_fields=['date_completed'])
