from django import forms
from dataclasses import dataclass


@dataclass
class VFormNonFieldDataC:
    code: str
    widget_class: str = ''


@dataclass
class VFormFieldDataC:
    field: forms.Field
    key: str
    label: str
    is_required: bool
    help_text: str = ''
    initial: object = None
    widget: object = None
    extra: VFormNonFieldDataC = None


