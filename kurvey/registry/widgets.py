from django import forms
from kommons.utils.generic import jsonify

from kurvey.registry.vforms import VForm
from kurvey.registry.utils import VFieldUtils


class FormsetWidget(forms.widgets.Widget):
    template_name = 'kurvey/values/formset_tabular.html'
    vfield = None
    expected_questions = None

    class Media(forms.widgets.Media):
        js = (
            'kurvey/plugins/jquery/jquery.min.js',
            'kurvey/plugins/jquery.formset/jquery.formset.js',
            # 'kurvey/plugins/datetimepicker/jquery.datetimepicker.js',
        )
        css = {
            'all': ('kurvey/plugins/jquery.formset/jquery.formset.css',
                    # 'kurvey/plugins/datetimepicker/jquery.datetimepicker.css',
                    )
        }

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)

        vfield_utils = VFieldUtils()
        xs = []
        for obj in self.expected_questions:
            vfield = vfield_utils.get_vfield(obj, self.vfield.capture_answer)
            xs.append(vfield)

        vform_fields = [vfield_utils.gen_vform_field(x) for x in xs]

        class GenFormMetaClass(VForm):
            def __new__(cls, *args, **kwargs):
                kwargs['fields'] = vform_fields
                return VForm(*args, **kwargs)

        GenFormset = forms.formset_factory(GenFormMetaClass, extra=1)
        data = self.prepare_data(name, value)
        formset = GenFormset(data, prefix=name)

        context.update({
            'name': name,
            'formset': formset,
            'can_change': True
        })

        return context

    def value_from_datadict(self, data, files, name):
        total_forms = data.get('%s-TOTAL_FORMS' % name, 0)
        xs = []
        for x in range(int(total_forms)):
            tmp = {}
            for k, v in data.items():
                key = '%s-%s-' % (name, x)
                if k.startswith(key):
                    val_k = k.replace(key, '')
                    tmp[val_k] = v
            xs.append(tmp)

        return xs

    def prepare_data(self, name, value):
        data = None
        if value:
            if type(value) == str:
                value = jsonify(value)

            data = {
                '%s-TOTAL_FORMS' % name: len(value),
                '%s-INITIAL_FORMS' % name: '0',
                '%s-MAX_NUM_FORMS' % name: '1000'
            }

            for pos, row in enumerate(value):
                for k, v in row.items():
                    key = '%s-%s-%s' % (name, pos, k)
                    data[key] = v

        return data
