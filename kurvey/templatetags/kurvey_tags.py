# -*- coding: utf-8 -*-
from django import template

from kurvey.utils import get_question_values


register = template.Library()


@register.filter
def get_review_values(capture): 
    question_qs = capture.question.children.filter(is_active=True).order_by('sort')

    entry = capture.get_ext_object()
    if entry and entry.cell_site.site_owner:
        hide_filters = entry.cell_site.site_owner.extra.get('survey.capture', {}).get('hide', {})
        question_qs = question_qs.exclude(**hide_filters)

    return get_question_values(capture, question_qs)


@register.filter
def get_picture_caption(answer):
    html = ""
    if answer:
        html = "<small>%s</small><br />%s" % (answer.question.short_text, answer.value)

    return html
