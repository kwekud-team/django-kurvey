# Generated by Django 2.2.12 on 2020-08-24 10:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kurvey', '0004_capture_code'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='capture',
            options={'ordering': ('-date_created',)},
        ),
        migrations.AlterModelOptions(
            name='questionset',
            options={'ordering': ('sort',)},
        ),
        migrations.AddField(
            model_name='questionset',
            name='sort',
            field=models.PositiveSmallIntegerField(default=1000),
        ),
    ]
