from django.test import TestCase

QUESTIONS = [
    {'pk': 100, 'short_text': 'Personal Information', 'number': 1, 'question_type': 'question-set',
     'input_type': 'StringValue'},
    {'pk': 101, 'parent': 100, 'short_text': 'First name', 'number': 1, 'question_type': 'question',
     'input_type': 'StringValue'},
    {'pk': 102, 'parent': 100, 'short_text': 'Last name', 'number': 2, 'question_type': 'question',
     'input_type': 'StringValue'},
    {'pk': 103, 'parent': 100, 'short_text': 'Date of Birth', 'number': 3, 'question_type': 'question',
     'input_type': 'StringValue'},
    {'pk': 104, 'parent': 100, 'short_text': 'Are you and employee', 'number': 4, 'question_type': 'question',
     'input_type': 'ChoiceValue'},

    {'pk': 200, 'short_text': 'Contact Information', 'number': 2, 'question_type': 'question-set',
     'input_type': 'StringValue'},
    {'pk': 201, 'parent': 200, 'short_text': 'Email', 'number': 1, 'question_type': 'question',
     'input_type': 'StringValue'},
    {'pk': 202, 'parent': 200, 'short_text': 'Phone', 'number': 2, 'question_type': 'question',
     'input_type': 'StringValue'},
    {'pk': 203, 'parent': 200, 'short_text': 'Fax', 'number': 3, 'question_type': 'question',
     'input_type': 'StringValue'},

    {'pk': 300, 'short_text': 'Address Information', 'number': 3, 'question_type': 'question-set',
     'input_type': 'StringValue'},
    {'pk': 301, 'parent': 300, 'short_text': 'Street Address', 'number': 1, 'question_type': 'question',
     'input_type': 'StringValue'},
    {'pk': 302, 'parent': 300, 'short_text': 'Post Address', 'number': 2, 'question_type': 'question',
     'input_type': 'StringValue'},
    {'pk': 303, 'parent': 300, 'short_text': 'Country', 'number': 3, 'question_type': 'question',
     'input_type': 'StringValue'},
]