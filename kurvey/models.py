# -*- coding: utf-8 -*-
import uuid
from django.urls import reverse
from django.db import models
from django.conf import settings
from django.utils.safestring import mark_safe
from django.utils import timezone

from mptt.models import MPTTModel, TreeForeignKey
from mptt.managers import TreeManager

from kommons.utils.math import base36encode
from kurvey.managers import QuestionQuerySet


class AbstractModel(models.Model):
    guid = models.UUIDField(null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                   related_name='%(app_label)s%(class)s_created_by')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                    related_name='%(app_label)s%(class)s_modified_by')
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField('Active', default=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        return super(AbstractModel, self).save(*args, **kwargs)


class QuestionSet(AbstractModel):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    slug = models.CharField(max_length=250)
    name = models.CharField(max_length=250)
    sort = models.PositiveSmallIntegerField(default=1000)
    description = models.TextField(null=True, blank=True)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('sort',)
        unique_together = ('site', 'slug')

    def is_closed(self):
        is_closed = not self.is_active
        if not is_closed:
            if self.end_date and self.end_date < timezone.now():
                is_closed = True
        return is_closed


class Question(MPTTModel, AbstractModel):
    question_set = models.ForeignKey(QuestionSet, on_delete=models.CASCADE)
    step = models.PositiveSmallIntegerField(default=0)
    sort = models.PositiveSmallIntegerField(default=1000)
    number = models.CharField(max_length=50)
    short_text = models.CharField(max_length=250)
    long_text = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    help_text = models.CharField(max_length=250, null=True, blank=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    question_type = models.CharField(max_length=20)
    input_type = models.CharField(max_length=50)
    slug = models.CharField(max_length=250, null=True, blank=True)
    alias = models.CharField(max_length=250, null=True, blank=True)
    is_required = models.BooleanField(default=False)
    is_hidden = models.BooleanField(default=False)
    is_image_required = models.BooleanField(default=False)
    extra = models.JSONField("Extra information", default=dict, blank=True, help_text="Extra information (use on input type)")
    template_name = models.CharField(max_length=100, null=True, blank=True, default="")
    initial_value = models.TextField(null=True, blank=True)
    negative_value = models.CharField(max_length=50, null=True, blank=True)

    tree = TreeManager()
    objects = QuestionQuerySet.as_manager()

    def __str__(self):
        return self.get_text()

    class Meta:
        ordering = ('sort', 'question_type',)
        unique_together = ('question_set', 'number')

    class MPTTMeta:
        order_insertion_by = ('sort', 'question_type',)

    def get_text(self):
        return self.long_text or self.short_text

    def get_input_options(self):
        return self.extra
    get_input_options.short_description = 'Input Options'


class Capture(AbstractModel):
    code = models.CharField(max_length=50, null=True, blank=True)
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    question_set = models.ForeignKey(QuestionSet, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    notes = models.TextField(blank=True, null=True)
    ext_content_pk = models.IntegerField(null=True, blank=True)
    ext_content_str = models.CharField(max_length=100, null=True, blank=True)

    date_completed = models.DateTimeField(null=True, blank=True)
    date_validated = models.DateTimeField(null=True, blank=True)
    date_cancelled = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.code or self.pk)

    class Meta:
        ordering = ('-date_created',)

    def save(self, *args, **kwargs):
        super(AbstractModel, self).save(*args, **kwargs)
        if not self.code:
            Capture.objects.filter(pk=self.pk).update(code=self.gen_code())

    def gen_code(self):
        code = base36encode(self.pk).zfill(4)
        return f'CQ{code}'

    def get_absolute_url(self):
        url = reverse('survey:capture_detail', args=(self.pk,))
        if self.ext_content_str and self.ext_content_pk:    
            url += '?ext_content_str=%s&ext_content_pk=%s' % (self.ext_content_str, self.ext_content_pk)
        return url

    def get_manage_url(self):
        return reverse('survey:capture_edit', args=(self.question_set.pk, self.pk,))

    def get_pdf_url(self):
        url = reverse('survey:capture_detail', args=(self.pk,))
        return '%s?action=print' % url

    def get_capture_pdf(self):
        html = '<a href="%s">View PDF</a>' % self.get_pdf_url()
        return mark_safe(html)
    get_capture_pdf.short_description = 'View'


class CaptureAnswer(AbstractModel):
    capture = models.ForeignKey(Capture, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    value = models.TextField()
    is_skipped = models.BooleanField(verbose_name='Skipped?', default=False)
    is_negative = models.BooleanField(verbose_name='Negative?', null=True)
    date_answered = models.DateTimeField(null=True, blank=True)
    notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return "%s: %s" % (self.capture, self.question)

    class Meta:
        ordering = ('question',)
        unique_together = ('capture', 'question')

    def get_processed_value(self):
        return self.value
