from enum import Enum


class KurveyK(Enum):
    QT_GROUP = 'qt-group'
    QT_QUESTION = 'question'
    QT_CHOICE = 'qt-choice'
    QT_SUB_QUESTION = 'qt-sub-question'
    QT_CUSTOM = 'qt-custom'


# INPUT_TYPES = {
#     'FORMSET': 'FormsetValue',
#     'STRING': 'StringValue',
# }
#
# VALUE_TYPES = (
#     'StringValue', 'ChoiceValue', 'IntegerValue', 'MultiValue',
#     'DocumentViewerValue', 'DisplayValue',
# )

# REFUSAL_CHOICES = (
#     ('unable-to-compute', 'Unable to compute value'),
#     ('do-not-know', 'I do not know'),
#     ('keep-private', 'Keep answer private'),
#     ('not-confortable-with-question', 'Not confortable with question'),
# )


# IGNORE_FORM_CONTROL_VALUES = ['Radio']