Hello,

Thank you for taking the time to complete the {{ capture.question_set.name }}.
You can review your submission by clicking on the following link: {{ capture_url }}

Thank you,
{{ capture.site.name }} Team.