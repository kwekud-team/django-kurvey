from django.urls import path
from kurvey import views

app_name = 'survey'


urlpatterns = [  
    path('questions/<int:question_set_pk>/', views.QuestionListView.as_view(), name='question_list'),
    path('capture/add/<int:question_set_pk>/', views.CaptureManageView.as_view(), name='capture_add'),
    path('capture/edit/<int:question_set_pk>/<int:pk>/', views.CaptureManageView.as_view(), name='capture_edit'),
    path('capture/detail/<int:pk>/', views.CaptureDetailView.as_view(), name='capture_detail'),
    path('capture/auth/<int:question_set_pk>/', views.CaptureAuthView.as_view(), name='capture_auth'),
    path('capture/start/<int:question_set_pk>/', views.CaptureStartView.as_view(), name='capture_start'),

    path('capture/list/<int:qs_pk>/', views.CaptureListView.as_view(), name='capture_list'),
    path('capture/list/', views.CaptureListView.as_view(), name='capture_list'),
]
