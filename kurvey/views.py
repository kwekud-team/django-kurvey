from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.contrib.auth import get_user_model
from django.contrib import auth
from django.template.loader import render_to_string
from django.urls import reverse
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormView
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.conf import settings
from django.core.mail import send_mail

from kommons.utils.http import get_request_site
from kommons.utils.pdf import render_to_pdf
from kurvey.models import QuestionSet, Question, Capture, CaptureAnswer
from kurvey.registry.utils import SurveyCaptureUtils
from kurvey.forms import CaptureAuthForm, CaptureListFilterForm
from kurvey.utils import EncodeUtils

USER_BACKEND = 'django.contrib.auth.backends.ModelBackend'


class QuestionSetDetailView(ListView):
    template_name = "kurvey/question_list.html"

    def get_queryset(self):
        question_set_pk = self.kwargs['question_set_pk']
        return Question.objects.active().filter(question_set__pk=question_set_pk)

    def get_context_data(self, **kwargs):
        context = super(QuestionListView, self).get_context_data(**kwargs)

        context.update({
        })
        return context


class QuestionListView(ListView):
    template_name = "kurvey/question_list.html"

    def get_queryset(self):
        question_set_pk = self.kwargs['question_set_pk']
        return Question.objects.active().filter(question_set__pk=question_set_pk)

    def get_context_data(self, **kwargs):
        context = super(QuestionListView, self).get_context_data(**kwargs)

        context.update({
        })  
        return context


class CaptureAuthView(FormView):
    form_class = CaptureAuthForm
    template_name = "kurvey/capture_auth.html"
    question_set = None

    def dispatch(self, *args, **kwargs):
        self.question_set = self.get_question_set()
        return super(CaptureAuthView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(CaptureAuthView, self).get_context_data(**kwargs)

        ctx.update({
            'question_set': self.question_set,
        })

        return ctx

    def form_valid(self, form):
        email = form.cleaned_data['email']

        user_model = get_user_model()

        user = user_model.objects.filter(username=email).first()
        if not user:
            user = get_user_model().objects.create_user(username=email, email=email, is_active=True)

        auth.login(self.request, user, backend=USER_BACKEND)

        self.success_url = reverse('kurvey:capture_add', args=(self.question_set.pk,))
        return super(CaptureAuthView, self).form_valid(form)

    def get_question_set(self):
        return QuestionSet.objects.filter(pk=self.kwargs['question_set_pk']).first()


class CaptureStartView(View):

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated:
            url = reverse('kurvey:capture_auth', args=(self.kwargs['question_set_pk'],))
            return HttpResponseRedirect(url)
        return super(CaptureStartView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        question_set = get_object_or_404(QuestionSet, pk=kwargs['question_set_pk'])
        site = question_set.site
        user = request.user

        capture = Capture.objects.filter(question_set=question_set, site=site, user=user, date_completed__isnull=True).first()
        if capture:
            messages.info(self.request, 'Existing record found. Kindly complete the form.')
            url = reverse('kurvey:capture_edit', args=(question_set.pk, capture.pk,))
        else:
            url = reverse('kurvey:capture_add', args=(question_set.pk,))

        return HttpResponseRedirect(url)


class CaptureManageView(FormView, LoginRequiredMixin):
    template_name = "kurvey/capture_manage.html"
    capture = None
    question_set = None
    survey_utils = None

    # @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated:
            url = reverse('kurvey:capture_auth', args=(self.kwargs['question_set_pk'],))
            return HttpResponseRedirect(url)

        self.question_set = self.get_question_set()
        if self.question_set and self.question_set.is_closed():
            pass

        self.capture = self.get_capture()
        self.survey_utils = SurveyCaptureUtils(self.request, self.question_set, capture=self.capture)
        return super(CaptureManageView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(CaptureManageView, self).get_context_data(**kwargs)

        ctx.update({
            'question_set': self.question_set,
            'capture': self.capture,
            'group_info': self.survey_utils.get_group_info()
        })

        return ctx

    def form_invalid(self, form):
        messages.error(self.request, 'Please rectify the form errors before proceeding.')
        return super().form_invalid(form)

    def form_valid(self, form):

        for x in form.fields:
            value = form.cleaned_data[x]

            if x.count('__') == 1:
                question_pk = x.split('__')[1]
                question = Question.objects.filter(pk=question_pk).first()
                if question:
                    capture_answer = CaptureAnswer.objects.update_or_create(
                        capture=self.capture,
                        question=question,
                        defaults={
                            'value': value or "",
                            'created_by': self.request.user,
                            'modified_by': self.request.user
                        })[0]

                    vfield = self.survey_utils.vfield_utils.get_vfield(question, capture_answer)
                    vfield.post_save()

        if not self.capture.code:
            self.capture.save()

        messages.success(self.request, "Saved successfully")
        return super(CaptureManageView, self).form_valid(form)

    def get_success_url(self):
        after_url = self.request.POST.get('_after_url', None)
        is_submitted = self.request.POST.get('_submit', None)
        next_url = self.request.GET.get('next', None)  # custom url, takes priority over after_url

        if is_submitted:
            Capture.objects.filter(pk=self.capture.pk).update(date_validated=timezone.now())
            next_url = self.capture.get_absolute_url()

        return next_url or after_url or self.request.get_full_path()

    def get_capture(self):
        capture_pk = self.kwargs.get('capture_pk', None)
        if capture_pk:
            obj = Capture.objects.filter(pk=capture_pk).first()
        else:
            site = self.question_set.site
            user = self.request.user

            obj = Capture.objects.get_or_create(
                question_set=self.question_set,
                site=site,
                user=user,
                date_completed__isnull=True,
                defaults={
                    'created_by': user,
                    'modified_by': user
                }
            )[0]

        return obj

    def get_question_set(self):
        return QuestionSet.objects.filter(pk=self.kwargs['question_set_pk']).first()

    def get_form(self, form_class=None):
        kwargs = self.get_form_kwargs()
        return self.survey_utils.get_form(**kwargs)


class CaptureListView(ListView):
    model = Capture
    question_sets = None
    question_set = None
    filter_class = CaptureListFilterForm

    # @method_decorator(staff_member_required(login_url=settings.LOGIN_URL))
    @method_decorator(staff_member_required)
    def dispatch(self, *args, **kwargs):
        self.question_sets = self.get_question_sets()
        self.question_set = self.get_question_set()
        return super(CaptureListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()
        qs = self.filter_class(self.request.GET, queryset=qs).qs
        if self.question_set:
            qs = qs.filter(question_set=self.question_set)
        if not self.request.user.is_superuser:
            qs = qs.exclude(user__is_superuser=True)
        return qs

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        ctx.update({
            'question_sets': self.question_sets,
            'question_set': self.question_set,
            'filter_form': self.get_filter_form()
        })
        return ctx

    def get_filter_form(self):
        return self.filter_class(self.request.GET, request=self.request).form
        # return self.filter_class(self.request).form

    def get_question_sets(self):
        site = get_request_site(self.request)
        return QuestionSet.objects.filter(site=site, is_active=True)

    def get_question_set(self):
        qs = self.get_question_sets()

        qs_pk = self.kwargs.get('qs_pk', None)
        if qs_pk:
            qs = qs.filter(pk=qs_pk)

        return qs.first()


class CaptureDetailView(DetailView):
    model = Capture
    template_name = "kurvey/capture_detail.html"
    template_pdf_name = "kurvey/capture_pdf.html"
    capture = None
    linked_object = None
    survey_utils = None

    # @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        self.capture = get_object_or_404(Capture, pk=kwargs['pk'])
        self.survey_utils = SurveyCaptureUtils(self.request, self.capture.question_set, capture=self.capture)
        if not self.get_can_view():
            return HttpResponseForbidden("You don't have permission to view this page.")

        return super(CaptureDetailView, self).dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        action = request.GET.get('action', None)
        capture = get_object_or_404(Capture, pk=kwargs['pk'])

        if action == 'complete':
            if not capture.date_completed:
                self.send_mail_admin(capture)
                self.send_mail_user(capture)
                self.survey_utils.mark_capture_as_complete()
                messages.success(self.request, f"{capture.question_set} completed successfully")

        elif action == 'send-mail':
            self.send_mail_user(capture)
            messages.success(self.request, "Mail sent successfully.")

        elif action == 'print':
            return render_to_pdf(self.template_pdf_name,
                                 {
                                     'pagesize': 'A4',
                                     'object': capture,
                                     'detail_object': self.linked_object or capture,
                                     'workspace': request.workspace,
                                     'detail_fields': self.get_detail_fields(self.linked_object),
                                     'review_values': self.survey_utils.get_capture_review(),
                                     'media_url': settings.MEDIA_URL
                                 }, request)

        if self.linked_object and not request.GET.get('stay'):
            url = self.linked_object.get_absolute_url()
            return HttpResponseRedirect(url)

        return super(CaptureDetailView, self).get(request, *args, **kwargs)

    def get_can_view(self):
        user = self.request.user

        if user.is_authenticated and (user.is_staff or self.capture.user == user):
            return True

        if not user.is_authenticated:
            # Try and get user from hashed key in email
            username = EncodeUtils().decode(self.request.GET.get('key'))
            if username:
                user = get_user_model().objects.filter(username=username, is_active=True).first()
                if user and self.capture.user == user:
                    auth.login(self.request, user, backend=USER_BACKEND)
                    return True

        return False

    def _send_mail(self, mail_data):
        from_email = settings.DEFAULT_FROM_EMAIL

        recipients = [x for x in mail_data['recipients'] if x]
        if recipients:
            send_mail(
                mail_data['subject'],
                mail_data['message'],
                from_email,
                recipients,
                fail_silently=True,
            )

    def send_mail_admin(self, capture):
        data = {
            'subject': f'{capture.site.name}: A user has completed {capture.question_set.name} on your website',
            'recipients': getattr(settings, 'KURVEY_CAPTURE_ADMINS', []),
            'message': render_to_string('kurvey/mail/capture_complete_admin.txt', context={
                'capture': capture,
                'capture_url': self.request.build_absolute_uri(capture.get_absolute_url())
            }),
        }
        return self._send_mail(data)

    def send_mail_user(self, capture):
        data = {
            'subject': f'{capture.site.name}: {capture.question_set.name} submitted successfully',
            'recipients': [capture.user.email],
            'message': render_to_string('kurvey/mail/capture_complete_user.txt', context={
                'capture': capture,
                'capture_url': f'{self.request.build_absolute_uri(capture.get_absolute_url())}?key={EncodeUtils().encode(capture.user.email)}'
            }),
        }
        return self._send_mail(data)

    def get_context_data(self, **kwargs):
        ctx = super(CaptureDetailView, self).get_context_data(**kwargs) 

        is_owner = (self.capture.user == self.request.user) or self.request.user.is_superuser

        ctx.update({
            'detail_fields': self.get_detail_fields(self.linked_object),
            'detail_object': self.linked_object or self.capture,
            'review_values': self.survey_utils.get_capture_review(),
            'is_owner': is_owner,
            'object': self.capture
        })
        return ctx

    def get_linked_object(self, capture):
        """
        External models will most likely have a model linking to the capture model. We check if such model
        exist and use it as the detail object instead. This will allow for a better degree of customization.
        """
        return None

    def get_detail_fields(self, linked_object):
        fields = ('code', 'user', 'date_created', 'date_completed',)
        
        if linked_object:
            fields = linked_object.Attr.capture_fields

        return fields
