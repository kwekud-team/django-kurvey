from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from kuploads.admin import KuploadsAdmin

from kurvey.models import QuestionSet, Question, Capture, CaptureAnswer
from kurvey.attrs import QuestionSetAttr, QuestionAttr, CaptureAttr, CaptureAnswerAttr
from kurvey.configs.uploads import QuestionExpImp


@admin.register(QuestionSet)
class QuestionSetAdmin(admin.ModelAdmin):
    fields = QuestionSetAttr.admin_fields
    list_display = QuestionSetAttr.admin_list_display
    list_filter = QuestionSetAttr.admin_list_filter
    list_editable = ['sort', 'is_active']
    prepopulated_fields = {"slug": ("name",)}


class QuestionInline(admin.TabularInline):
    model = Question
    extra = 1
    verbose_name = "Choice"
    verbose_name_plural = "Choices"
    fields = ['number', 'short_text']


@admin.register(Question)
class QuestionAdmin(MPTTModelAdmin, KuploadsAdmin):
    list_display = QuestionAttr.admin_list_display
    list_filter = QuestionAttr.admin_list_filter
    search_fields = ('question_type', 'input_type', 'short_text', 'slug', 'number')
    fieldsets = QuestionAttr.admin_fieldsets
    mptt_indent_field = "short_text"
    inlines = [QuestionInline]
    list_editable = ['is_required', 'is_active']
    exp_imp_class = QuestionExpImp

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
        obj.modified_by = request.user
        obj.save()
        return obj


class CaptureAnswerInline(admin.TabularInline):
    model = CaptureAnswer
    extra = 1


@admin.register(Capture)
class CaptureAdmin(admin.ModelAdmin):
    list_display = CaptureAttr.admin_list_display
    list_filter = CaptureAttr.admin_list_filter
    search_fields = ('scode',)
    inlines = [CaptureAnswerInline]
    actions = ['download_excel']

    excel_questions = {}


@admin.register(CaptureAnswer)
class CaptureAnswerAdmin(admin.ModelAdmin):
    list_display = CaptureAnswerAttr.admin_list_display
    list_filter = CaptureAnswerAttr.admin_list_filter
    fieldsets = CaptureAnswerAttr.admin_fieldsets
    search_fields = ['guid', 'value']
    readonly_fields = ['capture']


# @admin.register(CapturePicture)
# class CapturePictureAdmin(BaseAdmin):
#     list_display = CapturePictureAttr.admin_list_display
#     list_filter = CapturePictureAttr.admin_list_filter
#     fieldsets = CapturePictureAttr.admin_fieldsets
#     search_fields = ['guid',]
#     readonly_fields = ['capture', 'capture_answer', 'picture']
