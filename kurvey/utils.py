import logging
from io import BytesIO
import zipfile
import django.dispatch

from kommons.utils.model import get_str_from_model
from kurvey.models import Question, Capture
from kurvey.constants import KurveyK


survey_capture_created = django.dispatch.Signal()


def get_question_values(capture, group_qs):
    group_values = []
    capture_pictures = get_capture_pictures(capture)

    for grp in group_qs:
        question_values = []
        qs = grp.get_descendants(include_self=True)

        for x in qs.filter(question_type=KurveyK.QT_QUESTION.value, is_hidden=False, is_active=True):

            value_obj = x.get_input_obj(capture=capture)
            if value_obj:
                value = value_obj.get_display_value()

                pictures = []
                if value_obj.capture_answer:
                    pictures = list(filter(lambda x: x['capture_answer'] == value_obj.capture_answer.pk, capture_pictures))

                question_values.append({
                    'question': x,
                    'capture_answer': value_obj.capture_answer,
                    'value': value,
                    'text': x.long_text or x.short_text,
                    'pictures': pictures
                })

        if question_values:
            group_values.append({
                'group': grp,
                'question_values': question_values
            })
    return group_values


def get_capture_pictures(capture):
    return []
    # pic_qs = CapturePicture.objects.filter(capture=capture)
    # values = pic_qs.values('id',
    #                        'capture_answer',
    #                        'capture_answer__question__number',
    #                        'capture_answer__question__short_text',
    #                        'picture__image')
    # return list(values)


def get_form_values(qs, capture=None, capture_answer=None):
    xs = []
    for x in qs:
        value_obj = x.get_input_obj(capture=capture, capture_answer=capture_answer)
        if value_obj: 

            if value_obj.capture_answer:
                is_skipped = value_obj.capture_answer.is_skipped
                notes = value_obj.capture_answer.notes
            else:
                is_skipped, notes = False, ""

            widget_attrs = value_obj._extra_kwargs.get('widget_attrs', {})
            widget_attrs.update({
                'number': x.number, 
                'is_skipped': is_skipped,
                'notes': notes,
            })

            xs.append({
                'key': x.pk,
                'value_obj': value_obj,
                'non_fields_args': widget_attrs,
            })
    return xs


class ExcelBuilder(object):

    def __init__(self, question_sets, capture_qs):
        self.question_sets = question_sets
        self.capture_qs = capture_qs

    def execute(self):
        sets = []
        for key, slugs in self.question_sets.items():
            main_question = self.get_question(key)
            rows = []
            if main_question:
                headers, cell_data = [], []
                qs = self.capture_qs.filter(question=main_question)
                for row_pos, capture in enumerate(qs):
                    tmp_hdr, tmp_data = [], []
                    children_qs = main_question.children.filter(is_active=True)
                    # for pos_col, y in enumerate(slugs):
                    for pos_col, col_question in enumerate(children_qs):
                        opts = {}  # self.get_options(col_question.slug)
                        opts.update({'row_pos': row_pos, 'col_pos': pos_col})
                        # col_question = self.get_question(opts['slug'])
                        # ll=Question.objects.filter(slug=opts['slug'])

                        if col_question:
                            if row_pos == 0: 
                                hdr = self.get_header(col_question, opts, row_pos, pos_col)
                                headers.append(hdr)

                            value = self.get_data(col_question, capture, opts)
                            tmp_data.append(value)

                    cell_data.append(tmp_data)

                hdrs = self.process_headers(headers)
                
                sets.append({
                    'main': main_question,
                    'rows': cell_data,
                    'headers': hdrs
                })
        # aa
        return sets

    def get_question(self, slug):
        return Question.objects.get_or_none(is_active=True, slug=slug)

    def get_options(self, val):
        dt = {}

        if type(val) == dict:
            dt.update(val)
        else:
            dt = {
                'slug': val,
            }

        return dt

    def get_header(self, question, options, row_pos=0, col_pos=0, level=0):
        dt = {}
        hdr_txt = options.get('label', None) or question.get_text()
        parent = options.get('parent', None) 

        key = 'hdr__%s__%s' % (level, col_pos) 
        dt[key] = hdr_txt   
        if parent: 
            opts = self.get_options(parent)
            col_question = self.get_question(opts['slug']) 

            level += 1
            parent_key = 'hdr__%s__%s' % (level, col_pos) 
            dt[parent_key] = self.get_header(col_question, opts, row_pos=row_pos, col_pos=col_pos)[key]
        else:
            dt[key] = hdr_txt   
        
        return dt

    def get_data(self, question, capture, options):
        ca_obj = capture.captureanswer_set.get_or_none(question=question)

        if ca_obj:
            in_obj = ca_obj.get_input_obj() 
            
            func_name = options.get('function', None)
            if func_name == 'xyz':
                func = getattr(self, 'exec_custom_%s' % func_name, None)
                val = func(in_obj, options)  
            else:
                val = in_obj.get_display_value()
        else:
            val = '' 

        return val

    def process_headers(self, headers):
        dt = {}
        for posx, x in enumerate(headers):
            for pos, (k, v) in enumerate(x.items()):
                junk, level, position = k.split('__')
                level, position = int(level), int(position)

                if level not in dt:
                    dt[level] = {}

                dt[level][position] = v

        # Get largest vals for all levels.
        largest_key = 0
        largest_val = 0
        for key, vals in dt.items():
            size = len(vals)
            if size > largest_val:
                largest_val = size
                largest_key = key

        # We iterate through all levels and fill empty indexes with empty string
        new_dt = {}
        adj_cnt = {}
        for key, vals in dt.items():
            for cnt in range(largest_val):
                new_val = vals.get(cnt, '') 
                already_assigned = False

                if key not in new_dt:
                    new_dt[key] = []
                    adj_cnt[key] = 0

                prev_cnt = cnt - adj_cnt[key] - 1
                # prev_cnt = (1000 if prev_cnt < 0 else prev_cnt)
                # Group similar header names into list
                try:
                    prev_val = new_dt[key][prev_cnt]
                except IndexError:
                    prev_val = None 

                if prev_val:
                    if type(prev_val) == list:
                        xs = prev_val
                        prev_val = prev_val[0]
                    else:
                        xs = [prev_val]

                    if prev_val == new_val:
                        xs.append(new_val)
                        new_dt[key][prev_cnt] = xs 
                        adj_cnt[key] += 1
                        already_assigned = True

                if not already_assigned:
                    new_dt[key].append(new_val) 

        # aa
        # Inverse sort by keys because higher levels needs to come first in headers
        keys = list(new_dt.keys())
        keys.sort(reverse=True)
        xs = []
        for x in keys:
            xs.append( new_dt[x] )
        
        return xs


def check_surveyed(question, ext_object):
    app_model = get_str_from_model(ext_object)

    qs = Capture.objects.filter(question=question, ext_content_str=app_model, ext_content_pk=ext_object.pk)

    obj = (qs[0] if qs.exists() else None)
    date_completed = (obj.date_completed if obj else None)

    return {
        'capture': obj,
        'date_completed': date_completed,
    }


def zip_in_memmory(file_groups):
    s = BytesIO()

    zf = zipfile.ZipFile(s, "w")

    for fil in file_groups:
        zf.writestr(fil['filename'], fil['content'])

    zf.close()

    return s.getvalue()


import base64


class EncodeUtils:

    def encode(self, text):
        text_bytes = text.encode('ascii')
        base64_bytes = base64.b64encode(text_bytes)
        return base64_bytes.decode('ascii')

    def decode(self, text):
        if text:
            base64_bytes = text.encode('ascii')
            try:
                message_bytes = base64.b64decode(base64_bytes)
                return message_bytes.decode('ascii')
            except Exception as e:
                logging.error(str(e))
        return ''
