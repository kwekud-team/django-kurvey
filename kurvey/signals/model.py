from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone


@receiver(post_save)
def save_v_objects(sender, instance, created, **kwargs):

    if sender == Picture and instance.ext_content_str == CaptureAnswer().app_model():
        capture_answer = CaptureAnswer.objects.get_or_none(guid=instance.ext_content_guid)
        if capture_answer:
            CapturePicture.objects.get_or_update(workspace=capture_answer.workspace, capture_answer=capture_answer,
                                                 capture=capture_answer.capture, picture=instance)
            Picture.objects.filter(pk=instance.pk).update(date_linked=timezone.now())
        else:
            Picture.objects.filter(pk=instance.pk).update(is_active=False)
