from urllib import parse

from kommons.utils.http import get_request_site
from kommons.utils.generic import boolify, jsonify
from kuploads.generators.base import ModelExpImp, ModelExporter, ModelImporter
from kurvey.models import QuestionSet, Question
from kurvey.constants import KurveyK
from kurvey.forms import QuestionImporterForm


class QuestionExporter(ModelExporter):
    model = Question
    export_fields = ['number', 'parent', 'short_text',  'question_type', 'input_type', 'get_input_options',
                     'is_required', 'is_image_required', 'help_text']


class QuestionImporter(ModelImporter):
    form_class = QuestionImporterForm

    def __init__(self, request, model, upload_setup):
        super(QuestionImporter, self).__init__(request, model, upload_setup)
        self.site = get_request_site(request)

    def get_question_set(self, upload_file):
        question_set_pk = upload_file.upload_log.extra.get('data', {}).get('question_set_pk', None)
        if question_set_pk:
            return QuestionSet.objects.filter(pk=question_set_pk).first()

    def pre_run(self, upload_file, current_sheet):
        dt = super(QuestionImporter, self).pre_run(upload_file, current_sheet)

        question_set = self.get_question_set(upload_file)
        if question_set:
            Question.objects.filter(question_set=question_set).update(is_active=False)
            dt['question_set'] = question_set

        return dt

    def save_commit(self, upload_file, row_data, row_index, pre_run_results):
        number_str = row_data['number']
        parent_str = row_data['parent']
        text_str = row_data['short_text']
        question_type_str = row_data['question_type']
        input_type_str = row_data['input_type']
        input_options_str = row_data['get_input_options']
        required_str = row_data['is_required']
        image_required_str = row_data['is_image_required']
        help_text_str = row_data['help_text']

        question_set = pre_run_results['question_set']

        if question_set and number_str:
            number_str = str(number_str).replace(".0", "")
            input_type = f'V{input_type_str}'
            is_required = boolify(required_str)
            is_image_required = boolify(image_required_str)

            question_type = (question_type_str if question_type_str else KurveyK.QT_QUESTION.value).lower()

            parent = None
            if parent_str:
                parent = Question.objects.filter(question_set=question_set, number=parent_str, is_active=True).first()

            extra = {}
            if input_options_str:
                extra = jsonify(input_options_str)

            if input_type_str == 'Formset':
                # options = parse.parse_qsl(input_options_str)
                extra.update({
                    'formset': {
                        'widget': 'components.survey.values.widgets.FormsetWidget',
                        'source': 'QUERYSET',
                        # 'options': dict(options)
                    }
                })

            Question.objects.update_or_create(
                question_set=question_set,
                number=number_str,
                defaults={
                    'is_active': True,
                    'parent': parent,
                    'sort': row_index,
                    'short_text': text_str,
                    'question_type': question_type,
                    'input_type': input_type,
                    'help_text': help_text_str,
                    'extra': extra,
                    'is_required': is_required,
                    'is_image_required': is_image_required,
                    'created_by': self.request.user,
                    'modified_by': self.request.user,
                })

        Question.tree.rebuild()


class QuestionExpImp(ModelExpImp):
    model = Question
    exporter_class = QuestionExporter
    importer_class = QuestionImporter
