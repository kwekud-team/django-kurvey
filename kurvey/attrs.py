
class QuestionSetAttr:
    admin_fields = ('site', 'name', 'slug', 'description', 'start_date', 'end_date', 'created_by', 'modified_by',)
    admin_list_display = ('slug', 'name', 'description', 'start_date', 'end_date', 'site', 'sort', 'is_active')
    admin_list_filter = ('start_date', 'end_date',)


class QuestionAttr:
    admin_list_display = ('number', 'sort', 'short_text', 'question_type', 'input_type', 'parent', 'is_active', 'is_required',
                          'question_set')
    admin_list_filter = ('question_set', 'question_type', 'input_type', 'is_active', 'is_required')
    admin_fieldsets = [
        ('Basic',
            {'fields': ('question_set', 'slug', 'parent', 'step', 'number', 'short_text', 'long_text', 'help_text',
                        'question_type', 'input_type', )}),
        ('Details',
            {'fields': ('sort', 'template_name', 'description', 'extra', 'initial_value', 'is_active',
                        'is_required')}),
    ]
    serializer_fields = ['scode', 'number', 'sort', 'short_text', 'description', 'long_text', 'question_type',
                         'input_type', 'parent', 'extra', 'is_required', 'help_text']


class CaptureAttr:
    admin_list_display = ('pk', 'question_set', 'user', 'date_completed', 'date_validated', 'date_cancelled', 'ext_content_str',
                          'ext_content_pk')
    admin_list_filter = ('date_created', 'date_completed', 'date_validated', 'date_cancelled', 'is_active')
    serializer_fields = ['scode', 'user', 'date_completed', 'notes', 'question', 'ext_content_pk', 'ext_content_str']


class CaptureAnswerAttr:
    admin_list_display = ('pk', 'capture', 'question', 'value', 'is_skipped', 'is_negative',)
    admin_list_filter = ('date_created', 'is_active', 'is_negative')
    admin_fieldsets = [
        ('Basic',
            {'fields': ('scode', 'question',)}),
        ('Details',
            {'fields': ('value', 'is_skipped', 'date_answered', 'notes')}),
    ]
    serializer_fields = ['scode', 'capture', 'question', 'value', 'is_skipped', 'notes', 'date_answered']
