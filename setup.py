import setuptools

long_description = ''
try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except FileNotFoundError:
    pass

setuptools.setup(
     name='kurvey',
     version='0.1.14',
     author="KDanso",
     author_email="dev@kkdanso.com",
     description="Questionnaire application for django",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://bitbucket.org/kwekud-team/django-kurvey",
     packages=setuptools.find_packages(),
     include_package_data=True,
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
     install_requires=[
        'django-mptt>=0.11.0',
        'jsonfield2>=3.0.3',
        'django-filter>=2.3.0',
     ]
)
